package ls02_5_01;

import java.util.ArrayList;

/**
 * 
 * Dies ist die Klasse Raumschiff von der LS02.
 * 
 * @author Jonathan Anders
 * @version 6.0
 *
 */

public class Raumschiff {

	/**
	 * 
	 * Als erstes werden die Atribute gesetzt.
	 * 
	 */
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent,
			int zustandSchildeInProzent, int zustandHuelleInProzent,
			int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden,
			String schiffsname) {

		

	}

	/**
	 * 
	 * Die getPhotonentorpedoAnzahl Methode erhaelt die Anzahl der Photonentorpedos.
	 * 
	 * @return photonentorpedoAnzahl
	 * 
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * 
	 * Die setPhotonentorpedoAnzahl Methode setzt die Anzahl der Photonentorpedos.
	 * 
	 * @param
	 * 
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	/**
	 * 
	 * Die getEnergieversorgungInProzent Methode erhaelt die Energieversorgung.
	 * 
	 * @return energieversorgungInProzent
	 * 
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * 
	 * Die Methode setEnergieversorgungInProzent setzt den Wert der Energieversorgung.
	 * 
	 * @param
	 * 
	 */
	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}

	/**
	 * 
	 * Die Methode getSchildeInProzent erhaelt den Wert der Schilde.
	 * 
	 * @return schildeInProzent
	 * 
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * 
	 * Die setSchildeInProzent Methode setzt den Wert der Schilde.
	 * 
	 * @param
	 * 
	 */
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}

	/**
	 * 
	 * Die Methode getHuelleInProzent erhaelt den Wert der Schilde.
	 * 
	 * @return huelleInProzent
	 * 
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * 
	 * Die setHuelleInProzent Methode setzt den Wert der Huelle.
	 * 
	 * @param
	 * 
	 */
	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}

	/**
	 * 
	 * Die Methode getLebenserhaltungssystemeInProzent erhaelt den Wert der Lebenserhaltungssysteme.
	 * 
	 * @return lebenserhaltungssystemeInProzent
	 * 
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * 
	 * Die setLebenserhaltungssystemeInProzent Methode setzt den Wert der Lebenserhaltungssysteme.
	 * 
	 * @param
	 * 
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	/**
	 * 
	 * Die Methode getAndroidenAnzahl erhaelt die Anzahl der Androiden.
	 * 
	 * @return androidenAnzahl
	 * 
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * 
	 * Die setAndroidenAnzahl Methode setzt die Anzahl der Androiden.
	 * 
	 * @param
	 * 
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * 
	 * Die Methode getSchiffsname erhaelt den Schiffsnamen.
	 * 
	 * @return schiffsname
	 * 
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * 
	 * Die setSchiffsname Methode setzt den Schiffsnamen.
	 * 
	 * @param
	 * 
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	 * 
	 * Die addLadung Methode fuegt Ladung hinzu.
	 * 
	 * @param
	 * 
	 */
	public void addLadung(Ladung neueLadung) {

	}

	/**
	 * 
	 * photonentorpedoSchiessen ist verantwortlich fuer das Abschiessen von Photonentorpedos auf das Raumschiff r.
	 * 
	 * @param
	 * 
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {

		//Setzen der Atribute
		photonentorpedoAnzahl = getPhotonentorpedoAnzahl();

		//Ausgabe
		if(photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl--;
			System.out.println("Photonentorpedo abgeschossen!");
			treffer(r);
		} else if(photonentorpedoAnzahl < 1) {
			System.out.println("-=*Click*=-");
		}

	}

	/**
	 * 
	 * phaserkanonenSchiessen ist verantwortlich fuer das Abschiessen von Phaserkanonen auf das Raumschiff r.
	 * 
	 * @param
	 * 
	 */
	public void phaserkanonenSchiessen(Raumschiff r) {

		//Setzen der Atribute
		energieversorgungInProzent = getEnergieversorgungInProzent();

		//Ausgabe
		if(energieversorgungInProzent > 50) {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			System.out.println("Phaserkanone abgeschossen!");
			treffer(r);
		} else if(photonentorpedoAnzahl < 50) {
			System.out.println("-=*Click*=-");
		}

	}

	/**
	 * 
	 * treffer ist verantwortlich fuer die/den Treffer von Raumschiff r.
	 * 
	 * @param
	 * 
	 */
	private void treffer(Raumschiff r) {

		schildeInProzent = schildeInProzent - 50;
		if(schildeInProzent < 1) {
			huelleInProzent = huelleInProzent - 50;
			energieversorgungInProzent = energieversorgungInProzent - 50;
			if(huelleInProzent < 1) {
				lebenserhaltungssystemeInProzent = 0;
				System.out.println("Lebenserhaltungssysteme von " + r.getSchiffsname() + " wurden vernichtet!");
			}
		}
		System.out.println(r.getSchiffsname() + " wurde getroffen!");

	}

	/**
	 * 
	 * nachrichtAnAlle ist verantwortlich fuer das Verschicken von Nachrichten an Alle.
	 * 
	 * @param
	 * 
	 */
	public void nachrichtAnAlle(String message) {

		//broadcastKommunikator.add(message);
		System.out.println("Nachricht an Alle: \n" + message);

	}

	/**
	 * 
	 * eintraegeLogbuchZurueckgeben ist verantwortlich fuer das zurueckgeben von den Logbucheintraegen.
	 * 
	 * @return
	 * 
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;

	}

	/**
	 * 
	 * photonentorpedosLaden ist verantwortlich fuer das Laden von Photonentorpedos.
	 * 
	 * @param
	 * 
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {

	}

	/**
	 * 
	 * reperaturDurchfuehren ist verantwortlich fuer das Durchfueren von Reperaturen.
	 * 
	 * @param
	 * 
	 */
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
			boolean schiffshuelle, int anzahlDroiden) {

	}

	/**
	 * 
	 * zustandRaumschiff ist verantwortlich fuer das Ausgeben vom Zustand des Raumschiffes.
	 * 
	 */
	public void zustandRaumschiff() {

		//Setzen der Atribute
		schiffsname = getSchiffsname();
		photonentorpedoAnzahl = getPhotonentorpedoAnzahl();
		energieversorgungInProzent = getEnergieversorgungInProzent();
		schildeInProzent = getSchildeInProzent();
		huelleInProzent = getHuelleInProzent();
		lebenserhaltungssystemeInProzent = getLebenserhaltungssystemeInProzent();
		androidenAnzahl = getAndroidenAnzahl();


		//Ausgabe
		System.out.println("|--| " + schiffsname + ": ");
		System.out.println("|");
		System.out.println("|----| Photonentorpedo Anzahl: " + photonentorpedoAnzahl);
		System.out.println("|");
		System.out.println("|----| Energieversorgung (in %): " + energieversorgungInProzent);
		System.out.println("|");
		System.out.println("|----| Schilde (in %): " + schildeInProzent);
		System.out.println("|");
		System.out.println("|----| Huelle (in %): " + huelleInProzent);
		System.out.println("|");
		System.out.println("|----| Lebenserhaltungssysteme (in %): " + lebenserhaltungssystemeInProzent);
		System.out.println("|");
		System.out.println("|----| Androiden Anzahl: " + androidenAnzahl);

	}

	/**
	 * 
	 * Die Methode getLadungsverzeichnis erhaelt das Ladungsverzeichnis.
	 * 
	 * @return ladungsverzeichnis
	 * 
	 */
	public ArrayList<Ladung> getLadungsverzeichnis(){
		return this.ladungsverzeichnis;
	}

	/**
	 * 
	 * Die setLadungsverzeichnis Methode setzt das Ladungsverzeichnis.
	 * 
	 * @param
	 * 
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	/*public void ladungsverzeichnisAusgeben() {

		System.out.println(getLadungsverzeichnis());

	}*/

	/**
	 * 
	 * ladungsverzeichnisAufraeumen ist verantwortlich fuer das Aufraeumen vom Ladungsverzeichnis.
	 * 
	 */
	public void ladungsverzeichnisAufraeumen() {

	}
}
