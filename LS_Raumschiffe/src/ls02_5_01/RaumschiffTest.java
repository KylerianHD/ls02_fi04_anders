package ls02_5_01;

/**
 * 
 * Dies ist die Klasse RaumschiffTest von der LS02.
 * 
 * @author Jonathan Anders
 * @version 6.0
 *
 */
public class RaumschiffTest {

public static void main(String[] args) {
	
	/**
	 * 
	 * Als erstes werden die Atribute gesetzt.
	 * 
	 */
	Raumschiff klingonen = new Raumschiff();
	Raumschiff romulaner = new Raumschiff();
	Raumschiff vulkanier = new Raumschiff();
	
	Ladung klingonenladung = new Ladung();
	Ladung romulanerladung = new Ladung();
	Ladung vulkanierladung = new Ladung();
	
	//Atribute Klingonen
	klingonen.setSchiffsname("IKS Hegh'ta");
	klingonen.setPhotonentorpedoAnzahl(1);
	klingonen.setEnergieversorgungInProzent(100);
	klingonen.setSchildeInProzent(100);
	klingonen.setHuelleInProzent(100);
	klingonen.setLebenserhaltungssystemeInProzent(100);
	klingonen.setAndroidenAnzahl(2);
	klingonenladung.setBezeichnung("Ferengi Schneckensaft");
	klingonenladung.setMenge(200);
	klingonenladung.setBezeichnung("Bat'leth Klingonen Schwert");
	klingonenladung.setMenge(200);
	
	//Atribute Romulaner
	romulaner.setSchiffsname("IRW Khazara");
	romulaner.setPhotonentorpedoAnzahl(2);
	romulaner.setEnergieversorgungInProzent(100);
	romulaner.setSchildeInProzent(100);
	romulaner.setHuelleInProzent(100);
	romulaner.setLebenserhaltungssystemeInProzent(100);
	romulaner.setAndroidenAnzahl(2);
	romulanerladung.setBezeichnung("Borg-Schrott");
	romulanerladung.setMenge(5);
	romulanerladung.setBezeichnung("Rote Materie");
	romulanerladung.setMenge(2);
	romulanerladung.setBezeichnung("Plasma-Waffe");
	romulanerladung.setMenge(50);
	
	//Atribute Vulkanier
	vulkanier.setSchiffsname("Ni'Var");
	vulkanier.setPhotonentorpedoAnzahl(0);
	vulkanier.setEnergieversorgungInProzent(80);
	vulkanier.setSchildeInProzent(80);
	vulkanier.setHuelleInProzent(50);
	vulkanier.setLebenserhaltungssystemeInProzent(100);
	vulkanier.setAndroidenAnzahl(5);
	vulkanierladung.setBezeichnung("Forschungssonde");
	vulkanierladung.setMenge(35);
	vulkanierladung.setBezeichnung("Photonentorpedo");
	vulkanierladung.setMenge(3);
	
	//Ablauf
	/**
	 * 
	 * Dann kommt der Ablauf.
	 * 
	 */
	//Klingonen schiessen Photonentorpedos 
	klingonen.photonentorpedoSchiessen(romulaner);
	
	//Romulaner schiessen Phaserkanonen
	romulaner.phaserkanonenSchiessen(klingonen);
	
	//Vulkanier schicken Nachricht an Alle
	vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
	
	/*
	  Klingonen rufen Zustand Ihres Raumschiffes ab,
	  geben Ihr Ladungsverzeichnis aus und
	  schie�en mit zwei weiteren Photonentorpedos auf die Romulaner
	*/
	klingonen.zustandRaumschiff();
	klingonenladung.getBezeichnung();
	klingonenladung.getMenge();
	klingonen.photonentorpedoSchiessen(romulaner);
	klingonen.photonentorpedoSchiessen(romulaner);
	
	/*
	 Die Klingonen, die Romulaner und
	 die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und
	 geben Ihr Ladungsverzeichnis aus
	 */
	klingonen.zustandRaumschiff();
	klingonenladung.getBezeichnung();
	klingonenladung.getMenge();
	romulaner.zustandRaumschiff();
	romulanerladung.getBezeichnung();
	romulanerladung.getMenge();
	vulkanier.zustandRaumschiff();
	vulkanierladung.getBezeichnung();
	vulkanierladung.getMenge();
	
	}
	
}
