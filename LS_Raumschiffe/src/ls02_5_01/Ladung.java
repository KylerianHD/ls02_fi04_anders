package ls02_5_01;

/**
 * 
 * Dies ist die Klasse Ladung von der LS02.
 * 
 * @author Jonathan Anders
 * @version 6.0
 *
 */
public class Ladung {

	/**
	 * 
	 * Als erstes werden die Atribute gesetzt.
	 * 
	 */
	private String bezeichnung;
	private int menge;

	public Ladung() {

	}

	public Ladung(String bezeichnung, int menge) {

	}

	/**
	 * 
	 * Die getBezeichnung Methode erhaelt die Bezeichnung der Ladung.
	 * 
	 * @return bezeichnung
	 * 
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * 
	 * Die Methode setBezeichnung setzt die Bezeichnung der Ladung.
	 * 
	 * @param
	 * 
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * 
	 * Die getMenge Methode erhaelt die Menge der Ladung.
	 * 
	 * @return
	 * 
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * 
	 * Die Methode setMenge setzt die Menge der Ladung.
	 * 
	 * @param
	 * 
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}


}
