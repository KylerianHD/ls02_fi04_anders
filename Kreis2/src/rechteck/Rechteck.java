package rechteck;

public class Rechteck {

	private double seiteA;
	private double seiteB;
	
	public Rechteck(double seiteA, double seiteB) {
		setSeiteA(seiteA);
		setSeiteB(seiteB);
	}
	
	public double getSeiteA() {
		return seiteA;
	}
	
	public void setSeiteA(double seiteA) {
		this.seiteA = seiteA;
	}
	
	public double getSeiteB() {
		return seiteB;
	}
	
	public void setSeiteB(double seiteB) {
		this.seiteB = seiteB;
	}
	
	public double getFlaeche() {
		return this.seiteA * this.seiteB;
	}
	
	public double getUmfang() {
		return (2 * this.seiteA) + (2 * this.seiteB);
	}
}
