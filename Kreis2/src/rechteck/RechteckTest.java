package rechteck;

import java.util.Scanner;

public class RechteckTest {
	
	public static void main(String[] args) {
		Rechteck rechteck1 = new Rechteck(0, 0);
		//Rechteck rechteck2 = new Rechteck(0, 0);
		Scanner tastatur = new Scanner(System.in);
		
		
		//Setzen der Atribute
		System.out.println("Welchen Wert soll Seite a haben?: ");
		double userChoice = tastatur.nextDouble();
		rechteck1.setSeiteA(userChoice);
		
		System.out.println("Welchen Wert soll Seite b haben?: ");
		double userChoice2 = tastatur.nextDouble();
		rechteck1.setSeiteB(userChoice2);
		
		//Ausgabe
		System.out.println("Ihre Seite a: " + rechteck1.getSeiteA());
		System.out.println("Ihre Seite b: " + rechteck1.getSeiteB());
		System.out.println("Flaeche: " + rechteck1.getFlaeche());
		System.out.println("Umfang: " + rechteck1.getUmfang());
		
	}

}
